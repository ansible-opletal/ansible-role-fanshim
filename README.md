Fan Shim Ansible role
=========

## This role installs the python-fanshim library to control the Fanshim fans, as well as installs the systemd service

### Note: Molecule test will likely fail at this time as it's using Docker and fanshim relies on the platform being Raspberry Pi (obviously, lol)

Variables
=========

Check out vars/main.yml for configuration options (loosly follows [the official options](https://github.com/pimoroni/fanshim-python/tree/master/examples))

License
-------

MIT